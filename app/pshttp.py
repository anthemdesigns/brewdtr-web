# File: simplehttpserver-example-1.py

import SimpleHTTPServer
import SocketServer
import math
import random


# minimal web server.  serves files relative to the
# current directory.

# Randomize the port at runtime
PORT_RANGE = 9110

PORT = int(round( random.random()*100 + PORT_RANGE))

Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

httpd = SocketServer.TCPServer(("", PORT), Handler)

print "serving at port", PORT
httpd.serve_forever()
