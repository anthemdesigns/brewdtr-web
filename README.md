# BREW DTR Website - version 2

> Home of the Brew Coffee Bar (Raleigh) Website


## Overview

[BREW Coffee Bar Website](http://brewdtr.com) leverages the Google Web Starter Toolkit.

## Features

* Boilerplate version of [Google Web Starter Toolkit](http://developers.google.com/web/starter-kit)
* Responsive multi-device layout
* Uses [Flexbox](https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Flexible_boxes) to re-order sections based on screen orientation and size
* Maps and mobile-friendly links
* BREW Color Scheme
  - White | #FFFFFF
  - Black | #222222
  - Heather Gray | #333333
  - Light Gray | #CCCCCC


## Installation

- CLONE: `git clone git@bitbucket.org:jordie_yap/brewdtr-web.git`
- ZIP: [Download](https://bitbucket.org/jordie_yap/brewdtr-web/get/master.zip) the code and run with gulp or node (or simply add to existing web server like Apache or nginx).

* Everything inside the `app` folder should go into the root directory of your webserver folder.

## Manage

Manage content in the following sections (use text editor search/find feature):

- `navdrawer-container` - add new links (internal / external)
- `logo-img` - modify the logo file
- `location-section` - update location info / adjust map images
- `loc-info-list` - adjust operating hours / days
- `menu` - overall list of menu items and descriptions
- `coffee-menu-list[1|2]` - split list of items (put split where ever or wider screen views)
- `notcoffee-menu-list` - other beverages (shows up in third column in wide view)
- `beer-menu` - rotating beer draft and can list
- `social-connect` - links to social networking

## Browser Support

At present, we officially aim to support the following browsers:

* IE10, IE11, IE Mobile 10
* FF 30, 31
* Chrome 34, 35
* Safari 7, 8
* Opera 23, 24
* iOS Safari 7, 8
* Opera Coast
* Android / Chrome 4.4, 4.4.3
* Blackberry 1.0

This is not to say that the site cannot be used in browsers older than those reflected, but merely that our focus will be on ensuring our layouts work great in the above.

## License

Apache 2.0

Copyright 2014 Google Inc

[AnthemDesigns.com](anthemdesigns.com)
